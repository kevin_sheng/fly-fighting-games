import 'package:soul_summoner/langaw-game.dart';
import 'package:soul_summoner/components/fly.dart';
import 'package:flame/sprite.dart';
import 'dart:ui';

class HouseFly extends Fly {
  HouseFly(LangawGame game, double x, double y) : super(game) {
    flyRect = Rect.fromLTWH(x, y, game.tileSize * 1.7, game.tileSize * 1.7);
    flyingSprite = List();
    flyingSprite.add(Sprite('flies/house-fly-1.png'));
    flyingSprite.add(Sprite('flies/house-fly-2.png'));
    deadSprite = Sprite('flies/house-fly-dead.png');
  }
}
