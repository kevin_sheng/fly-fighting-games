import 'package:soul_summoner/langaw-game.dart';
import 'package:soul_summoner/components/fly.dart';
import 'package:flame/sprite.dart';
import 'dart:ui';

class MachoFly extends Fly {
  MachoFly(LangawGame game, double x, double y) : super(game) {
    flyRect = Rect.fromLTWH(x, y, game.tileSize * 2.0, game.tileSize * 2.0);
    flyingSprite = List();
    flyingSprite.add(Sprite('flies/macho-fly-1.png'));
    flyingSprite.add(Sprite('flies/macho-fly-2.png'));
    deadSprite = Sprite('flies/macho-fly-dead.png');
  }
}
