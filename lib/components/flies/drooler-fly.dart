import 'package:soul_summoner/langaw-game.dart';
import 'package:soul_summoner/components/fly.dart';
import 'package:flame/sprite.dart';
import 'dart:ui';

class DroolerFly extends Fly {
  DroolerFly(LangawGame game, double x, double y) : super(game) {
    flyRect = Rect.fromLTWH(x, y, game.tileSize * 1.1, game.tileSize * 1.1);
    flyingSprite = List();
    flyingSprite.add(Sprite('flies/drooler-fly-1.png'));
    flyingSprite.add(Sprite('flies/drooler-fly-2.png'));
    deadSprite = Sprite('flies/drooler-fly-dead.png');
  }
}
