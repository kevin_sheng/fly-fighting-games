import 'package:soul_summoner/langaw-game.dart';
import 'package:soul_summoner/components/fly.dart';
import 'package:flame/sprite.dart';
import 'dart:ui';

class HungryFly extends Fly {
  HungryFly(LangawGame game, double x, double y) : super(game) {
    flyRect = Rect.fromLTWH(x, y, game.tileSize * 1.9, game.tileSize * 1.9);
    flyingSprite = List();
    flyingSprite.add(Sprite('flies/hungry-fly-1.png'));
    flyingSprite.add(Sprite('flies/hungry-fly-2.png'));
    deadSprite = Sprite('flies/hungry-fly-dead.png');
  }
}
