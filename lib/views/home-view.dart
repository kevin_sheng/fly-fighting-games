import 'package:flutter/cupertino.dart';
import 'package:soul_summoner/langaw-game.dart';
import 'dart:ui';
import 'package:flame/sprite.dart';

class HomeView {
  LangawGame game;
  Rect titleRect;
  Sprite titleSprite;

  HomeView(this.game) {
    titleRect = Rect.fromLTWH(
        game.tileSize,
        (game.screenSize.height / 2) - game.tileSize * 4,
        game.tileSize * 7,
        game.tileSize * 4);
    titleSprite = Sprite('branding/title.png');
  }

  void update(double t) {}

  void render(Canvas c) {
    titleSprite.renderRect(c, titleRect);
  }
}
