import 'dart:ui';
import 'package:flame/flame.dart';
import 'package:flutter/gestures.dart';
import 'package:flame/game.dart';
import 'dart:math';
import 'components/background.dart';
import 'package:soul_summoner/components/flies/agile-fly.dart';
import 'package:soul_summoner/components/flies/drooler-fly.dart';
import 'package:soul_summoner/components/flies/house-fly.dart';
import 'package:soul_summoner/components/flies/hungry-fly.dart';
import 'package:soul_summoner/components/flies/macho-fly.dart';
import 'package:soul_summoner/View.dart';
import 'package:soul_summoner/views/home-view.dart';
import 'package:soul_summoner/components/start-button.dart';
import 'package:soul_summoner/views/lost-view.dart';
import 'package:soul_summoner/controller/spawner.dart';
import 'package:soul_summoner/components/help-button.dart';
import 'package:soul_summoner/components/credits-button.dart';
import 'package:soul_summoner/views/help-view.dart';
import 'package:soul_summoner/views/credits-view.dart';
import 'package:soul_summoner/components/score-display.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:soul_summoner/components/highscore-display.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:soul_summoner/components/music-button.dart';
import 'package:soul_summoner/components/sound-button.dart';

class LangawGame extends Game {
  //记录当前屏幕逻辑尺寸
  Size screenSize;
  // 这个实例变量等于屏幕宽度除以9
  double tileSize;
  List flies;
  Random rnd;
  Background background;
  View activeView = View.home;
  HomeView homeView;
  LostView lostView;
  StartButton startButton;
  bool isHandled = false;
  bool didHitAFly = false;
  FlySpawner spawner;
  HelpButton helpButton;
  CreditsButton creditsButton;
  HelpView helpView;
  CreditsView creditsView;
  int score;
  ScoreDisplay scoreDisplay;
  final SharedPreferences storage;
  HighscoreDisplay highscoreDisplay;
  AudioPlayer homeBGM;
  AudioPlayer playingBGM;
  MusicButton musicButton;
  SoundButton soundButton;

  LangawGame(this.storage) {
    initialize();
  }

  void initialize() async {
    flies = List();
    resize(await Flame.util.initialDimensions());
    background = Background(this);
    rnd = Random();
    homeView = HomeView(this);
    startButton = StartButton(this);
    lostView = LostView(this);
    spawner = FlySpawner(this);
    helpButton = HelpButton(this);
    creditsButton = CreditsButton(this);
    musicButton = MusicButton(this);
    soundButton = SoundButton(this);
    helpView = HelpView(this);
    creditsView = CreditsView(this);
    scoreDisplay = ScoreDisplay(this);
    score = 0;
    highscoreDisplay = HighscoreDisplay(this);
//背景音乐
    homeBGM = await Flame.audio.loopLongAudio('bgm/home.mp3', volume: .25);
    homeBGM.pause();
    playingBGM =
        await Flame.audio.loopLongAudio('bgm/playing.mp3', volume: .25);
    playingBGM.pause();

    playHomeBGM();
  }

  void playHomeBGM() {
    playingBGM.pause();
    playingBGM.seek(Duration.zero);
    homeBGM.resume();
  }

  void playPlayingBGM() {
    homeBGM.pause();
    homeBGM.seek(Duration.zero);
    playingBGM.resume();
  }

  @override
  void render(Canvas canvas) {
    background.render(canvas);
    flies.forEach((fly) {
      fly.render(canvas);
    });

    if (activeView == View.home) {
      homeView.render(canvas);
    }
    if (activeView == View.home || activeView == View.lost) {
      helpButton.render(canvas);
      creditsButton.render(canvas);
      startButton.render(canvas);
      musicButton.render(canvas);
      soundButton.render(canvas);
    }
    if (activeView == View.lost) {
      lostView.render(canvas);
    }
    if (activeView == View.help) helpView.render(canvas);
    if (activeView == View.credits) creditsView.render(canvas);
    if (activeView == View.playing) {
      highscoreDisplay.render(canvas);
      scoreDisplay.render(canvas);
    }
  }

  @override
  void update(double t) {
    spawner.update(t);
    flies.forEach((fly) => fly.update(t));
    flies.removeWhere((fly) => fly.isOffScreen);
    if (activeView == View.playing) scoreDisplay.update(t);
    // if ((n % 60) == 0) {
    //   print(n / 60);
    // }
    // n++;
  }

  ///父类的resize()实际上是空的, 如果我们不打算完全重写该功能, 需要调用一次父类的函数.
  ///实例变量是可从该类的所有函数中访问的变量. 比如我们可以调整它的大小, 并在render时获取该值.
  @override
  void resize(Size size) {
    print("屏幕改变了");
    screenSize = size;
    tileSize = screenSize.width / 9;
  }

  void onTapDown(TapDownDetails d) {
    bool didHitAFly = false;

    //弹窗
    if (!isHandled) {
      if (activeView == View.help || activeView == View.credits) {
        activeView = View.home;
        isHandled = true;
      }
    }

    //开始按钮
    if (!isHandled && startButton.rect.contains(d.globalPosition)) {
      if (activeView == View.home || activeView == View.lost) {
        startButton.onTapDown();
        isHandled = true;
      }
    }

    //点击小飞蝇
    if (!isHandled) {
      flies.forEach((fly) {
        //Rect类有一个好用的contains函数. 它接收offset(偏移量)作为参数, 若传递的offset在调用它的Rect的范围内, 返回true. 否则返回false.
        if (fly.flyRect.contains(d.globalPosition)) {
          fly.onTapDown();
          isHandled = true;
          didHitAFly = true;
        }
      });
      if (activeView == View.playing && !didHitAFly) {
        Flame.audio.play('sfx/haha' + (rnd.nextInt(5) + 1).toString() + '.ogg');
        playHomeBGM();
        activeView = View.lost;
      }
    }

    // 教程按钮
    if (!isHandled && helpButton.rect.contains(d.globalPosition)) {
      if (activeView == View.home || activeView == View.lost) {
        helpButton.onTapDown();
        isHandled = true;
      }
    }

    // 感谢按钮
    if (!isHandled && creditsButton.rect.contains(d.globalPosition)) {
      if (activeView == View.home || activeView == View.lost) {
        creditsButton.onTapDown();
        isHandled = true;
      }
    }
    // 音乐按钮
    if (!isHandled && musicButton.rect.contains(d.globalPosition)) {
      musicButton.onTapDown();
      isHandled = true;
    }

// 音效按钮
    if (!isHandled && soundButton.rect.contains(d.globalPosition)) {
      soundButton.onTapDown();
      isHandled = true;
    }

    isHandled = false;
    print('当前游戏状态');
    print(this.activeView);
  }

  void spawnFly() {
    double x = rnd.nextDouble() * (screenSize.width - tileSize * 2.0);
    double y = rnd.nextDouble() * (screenSize.height - tileSize * 2.0);

    //随机生成5种苍蝇
    switch (rnd.nextInt(5)) {
      case 0:
        flies.add(HouseFly(this, x, y));
        break;
      case 1:
        flies.add(DroolerFly(this, x, y));
        break;
      case 2:
        flies.add(AgileFly(this, x, y));
        break;
      case 3:
        flies.add(MachoFly(this, x, y));
        break;
      case 4:
        flies.add(HungryFly(this, x, y));
        break;
    }
  }
}
